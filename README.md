# Cloud Init

Cloud-init to bootstrap new servers to be managed by Ansible

## Usage

The easiest way is to include the config directly using

```
#include
https://gitlab.com/niek.tech/cloud-init/raw/master/cloud-init.yaml
```
